/**
  * This is the lightParse module, that realizes pattern matching on event streams. Refer to the docs or wiki for a detailed description of the supported pattern description language and details on the matching technique.
 * @example
 * //Start by defining the pattern and callback
 * var pattern = "SEQ(x1,x2,x3) WHERE (x1.time + 10000 > x2.time, x2.user = x3.user, x1.test = 'FAILURE', x2.test = 'SUCCESS', x3.test = 'FAILURE')";
 * var callback = function(args){
 *   console.log("Events matched for user " + args.x3.user);
 * }
 * //Set the number of users to 2. Required for correct behavior of user matching
 * lightparse.setNumberOfUsers(2);
 * //Add pattern with callback to lightParse
 * lightparse.addPattern(pattern, callback);
 * //To match events
 * var event = {user:1,...}
 * lightparse.matchEvent(event);
 * //Callback is evoked once a sequence of matching events was found
 * var event2 = {user:1,...}
 * lightparse.matchEvent(event2);
 * @namespace lightParse
 * @memberof MTLG
 */

var lightParse = (function () {

  /*
   * Part of the shared interface between the parser and handler is the automaton object.
   * This object contains the following attributes:
   * - states:
   *   * an object containing the state information. The information is accessible via the names specified in the SEQ-part.
   *   * This also includes the Start state.
   *   * State information includes:
   *      # nextState
   *      # previousState
   *      # condition: array of functions that return a boolean value. The parameter should be an object that contains all the events matched up to this point, with the event names as keys.
   *      # final: boolean that is true only for the final state
   *      # isUserSpecific: boolean that is true if there is some user comparison in the WHERE part that includes this attribute
   *      # interComparisons: Array containing the attributes that will be compared to attributes of other events that appear later in the sequence.
   *          The attributes are specified in arrays, where an attribute like test.someAttribute.a would be saved in ["test", "someAttribute", "a"]
   * - size:
   *   * The number of states including the Start state
   * - matchingStrategy:
   *   * A string specifiying the chosen matching strategy. Defaults to parallel.
   * - callback:
   *   * The callback function that is to be envoked once the automaton reaches a final state
   *
   */

  /*
   * Sub-module that performs the matching on automatons created by the parser.
   */
  var handler = (function () {
      var automata = []; //Array that contains all automata
      var buffer; //Used for general matching: match-buffer, saves up to maxBuffer events
      var maxBuffer = 100; //max number of events saved in match-buffer

      var timeoutTrigger = false; //This variable is set to true by the timeout function when the interval of a time-specific event has potentially ended.
                                  // This will result in special behaviour when the next event is matched (e.g. the state may be reversed)
      var timeInformation = []; //Array containing the information necessary to discern which timeout was triggered (e.g. which states do have to be reverted)

      var isParallelAutomaton = 1; //Remember matching strategy, this might be added to the automata later.
      var numberOfUsers = 4; //Stores the number of users for the parallel automaton

      /**
       * @function setNumberOfUsers
       * @description Sets the number of users to the parameter.
       * This is used to implement the parallel automaton when user specific event matching is required.
       * @param {number} newNumberOfUsers - the number of users that should be considered
       * @memberof MTLG.lightParse#
       */
      var setNumberOfUsers = function (newNumberOfUsers) {
        if(+newNumberOfUsers > 0) { //Check if number is valid
          numberOfUsers = +newNumberOfUsers;
        } else {
          console.log("Ignoring new number of users: " + newNumberOfUsers);
        }
      }


      /*
       * Initializes newly created parallel automata. This includes setting the currentState for all users if necessary.
       */
      var initializeParallelAutomaton= function(newAutomaton){
          newAutomaton.currentStates = []; //Begin with an empty array
          //This technique only requires isUserSpecific once, as early as possible. Deleting it everywhere else!
          //If the isUserSpecific attribute is true in all states and one of the conditions uses != for users, no event will match
          var currentStateLabel = "Start";
          var foundUserSpec = false;
          //Iterate through states, check if they are user specific
          while(newAutomaton.states[currentStateLabel]){
            if(!foundUserSpec && newAutomaton.states[currentStateLabel].isUserSpecific){//This is the first one
              foundUserSpec = true;
            } else if(foundUserSpec && newAutomaton.states[currentStateLabel].isUserSpecific){//User specific but not the first one
              newAutomaton.states[currentStateLabel].isUserSpecific = false;
            }
            currentStateLabel = newAutomaton.states[currentStateLabel].nextState;

          }
          if(foundUserSpec){ //There are user comparisons
            for(var i = 0; i < numberOfUsers; i++){
                newAutomaton.currentStates[i] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched".
            }
            //Initiate user array index mapping
            newAutomaton.userMapping = [];
          } else { //no need to parallize
            newAutomaton.currentStates[0] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched".
          }
      }

      /*
       * Initializes newly created general-matching automata.
       */
      var initializeGeneralAutomaton = function(newAutomaton){
          newAutomaton.currentStates = []; //Begin with an empty array
          var currentStateLabel = "Start";
          newAutomaton.currentStates[0] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched".
      }

      /*
       * This function checks if the provided event matches the user number, taking the mapping of the currentAutomaton into account.
       */
      //TODO this check for users has to be changed to check at the right position. This was not specified at the time of writing.
      var hasCorrectUser = function(event, userNumber, currentAutomaton){
        //extract user number TODO
        var user = event.User || event.user;

        //Check if user of event is new or old
        if(currentAutomaton.userMapping.indexOf(user) >= 0){ //mapping contains user
          return currentAutomaton.userMapping[userNumber] === user;
        } else if(currentAutomaton.userMapping.length < currentAutomaton.currentStates.length ){ //enumberOfUsers //new user
          currentAutomaton.userMapping.push(user);
          return currentAutomaton.userMapping[userNumber] === user;
        } else { //No more room for user, this indicates numberOfUsers was not set correctly
          console.log("Found user with ID: " + user + ", but there have already been " + numberOfUsers + " players. This event will be ignored"); //ERROR
          console.log(currentAutomaton.userMapping); //DEBUG
          return false;
        }
      }

      /**
       * @function deleteAutomaton
       * @description Deletes automaton at the specified array position.
       * Logs to console if the automaton did not exist.
       * @param {number} automatonIndex - the index of the automaton that is to be deleted
       * @memberof MTLG.lightParse#
       */
      var deleteAutomaton = function(automatonIndex){
          if(automatonIndex !== undefined && automatonIndex !== null && automata[automatonIndex]){
              delete automata[automatonIndex];
          } else {
              console.log("Tried removing nonexisting automaton at index: " + automatonIndex);
          }
      }

      /**
       * @function deleteAllAutomata
       * @description Deletes all automata by resetting the automata array to the empty array.
       * @memberof MTLG.lightParse#
       */
      var deleteAllAutomata = function(){
        automata = [];
      };


      /*
       * callback function, sets the timeoutTrigger to true.
       * This will result in a check for time-specific events in doStep when it is called next.
       */
      var activateTimeoutTrigger = function(){
          timeoutTrigger = true;
      }


      /*
       * Checks if currentState of automatonLabel, user is in transitive closure of firstName.
       */
      var isInTransitiveClosure = function(automatonLabel, user, firstName) {
        if(!automata[automatonLabel].states[firstName]){
            //Error
            return;
        }

        var currentName = firstName; //Set control variable to firstName
        var secondName = automata[automatonLabel].currentStates[user].at;
        if(firstName === secondName){ //if names are equal return true
            return true;
        }
        //Check all states after firstName, see if one of them is seconName
        while(automata[automatonLabel].states[currentName].nextState){
            if(automata[automatonLabel].states[currentName].nextState === secondName){
                return true;
            }
            currentName = automata[automatonLabel].states[currentName].nextState;
        }
        return false;
    }
      /*
       * Cleans up after reaching the final state in a parallel automaton. Resets currentState, deletes timeInformation and deletes matched events array.
       */
      var cleanupParallelFinal = function(currentAutomaton, currentUser, currentState){
          //Revert state, array of matched events
          currentState.at = "Start";
          currentState.matched = {};
          //Delete all timeSpecific info from the timeInformation array
          for(timeInfo in timeInformation){
              var currentInfo = timeInformation[timeInfo];
              if(currentInfo.automatonLabel === currentAutomaton && currentInfo.user === currentUser){
                  delete timeInformation[timeInfo];
              }
          }
      }

      var doParallelStep = function (currentAutomatonLabel,event){
        currentAutomaton = automata[currentAutomatonLabel];
        //Iterate over users, represented by the states in currentStates
        for(i in currentAutomaton.currentStates){
          //console.log(i); //DEBUG
          var currentState = currentAutomaton.currentStates[i];
          //console.log(currentState); //DEBUG

          //Check if event matches next state. matchEvent already sets the currentState if this is the case.
          if(matchEvent(event, currentAutomaton, currentAutomatonLabel, +i, currentAutomaton.states[currentState.at].nextState) ) {
            //Event matched next state

          } else { //match currentState again if time relevant
            //console.log("b"); //DEBUG
            //console.log(currentState); //DEBUG
            if(currentAutomaton.states[currentState.at].times){
              //console.log("c"); //DEBUG
              matchEvent(event, currentAutomaton, currentAutomatonLabel, +i, currentAutomaton.currentStates[i].at);
            }
          }
        }
      };



      /*
       * Tries to match event on automaton with user with the conditions of state
       */
      var matchEvent = function (event, automaton, automatonLabel, user, state){
          //console.log(automaton); //DEBUG
          //console.log(state); //DEBUG
          var currentState = automaton.currentStates[user];
          var myNextStateLabel = state; //Label of next state to be matched
          var myNextState = automaton.states[myNextStateLabel]; //Next State to be matched, contains condition
          //console.log(myNextStateLabel); //DEBUG
          //console.log(myNextState); // DEBUG
          var args = currentState.matched; //
          args[myNextStateLabel] = event;
          //console.log("args"); // DEBUG
          //console.log(args); // DEBUG
          //console.log(+user);
          //console.log(+user + 1);

          //Only match if user is correct (or not user specific)
          if(!myNextState.isUserSpecific || hasCorrectUser(event, user, automaton) ){
              //console.log("Correct User in matchEvent"); //DEBUG
              //now check if the array of conditions all return true
             var isConditionTrue = true; // Set to true to match events without conditions
             for (var j = 0; j < myNextState.condition.length; j++){
                isConditionTrue &= myNextState.condition[j](args);
             }
              if(isConditionTrue){ //go to next State
                  //console.log("Event matched! On automaton: " + automatonLabel + " and User: " + user + " for state: " + state); //DEBUG
                  currentState.at = myNextStateLabel;
                  if(myNextState.final){ //Final state?
                      //console.log("Reached a final state"); //DEBUG
                      //Clone matched events to pass them on as parameters to the callback function
                      var returnValue = JSON.parse(JSON.stringify(args));
                      automaton.callback(returnValue); //TODO: Parameters?

                      //Cleanup by resetting the state, timeInformation, ...
                      cleanupParallelFinal(automatonLabel, user, currentState);
                  } else { //No final state
                      if(myNextState.times){ //check if this event has some time constraints
                          //If this is the case, create the timeouts for all constraints
                          for(pairs in myNextState.times){
                              var currentPair = myNextState.times[pairs];
                              var time = currentPair.time;
                              var partner = currentPair.partner;
                              var startTime = (new Date()).getTime();
                              var endTime = startTime + time;

                              var currentStateLabel = automaton.states[currentState.at].previousState;
                              //Create object containing the information needed to revert the state if necessary
                              var timeObject = {"endTime" : endTime, "oldAt" : currentStateLabel, "automatonLabel": automatonLabel, "user" : user , "partner": partner};

                              //Delete old timeObject if it exists
                              for(timeInfo in timeInformation){
                                  var currentTimeInfo = timeInformation[timeInfo];
                                  if(currentTimeInfo.oldAt === timeObject.oldAt
                                     && currentTimeInfo.automatonLabel === timeObject.automatonLabel
                                    && currentTimeInfo.user === timeObject.user
                                    && currentTimeInfo.partner === timeObject.partner) {
                                      delete timeInformation[timeInfo];
                                      //console.log("Deleting old timeout"); //DEBUG
                                  }
                              }

                              //THEN push new timeObject
                              timeInformation.push(timeObject);

                              //console.log(timeInformation); //DEBUG
                              window.setTimeout(function(){
                                  activateTimeoutTrigger();
                              }, time);
                          }
                      }
                  }
                  return true; //Did match
              }
          }
          return false; //Did not match
      }


      var doGeneralStep = function(automatonLabel){
        //TODO
        //Ideas:
        //* Save incoming events in buffer if they would match and have new properties (use interAttribute to check)
        //* Create new state (do not change old one) where new event is matched.
        //* There are several approaches to the way the state information is kept:
        //  * Every transition-path is stored in its own state
        //  * Each state has a buffer of events that matched it (using some event of the past) - little space but more complicated to check if incoming events match
        //  * Each state remembers pairs, where the first point is the event that matched and the second point an array of "partners"
      }

      /*
       * Add a new automaton to the automata array and intitialize it according to its strategy.
       */
      var addAutomaton = function (newAutomaton){ //add the newly created automaton to the array of automata
        //Check for chosen matching strategy
        switch(newAutomaton.strategy){
          case "parallel": initializeParallelAutomaton(newAutomaton);
            break;
          case "general": initializeGeneralAutomaton(newAutomaton);
            break;
          default: //default to parallel
            initializeParallelAutomaton(newAutomaton);
        }
        automata[automata.length] = newAutomaton; //push new automaton into array
        return automata.length -1; //return the position of the new automaton in the array to make it possible to delete it later
      }


      /**
       * @function matchEvent
       * @description This function takes an event and adds it to the matched event stream by performing a step on all matching automata.
       * It automatically selects the strategy that was specified in the creation pattern.
       * @param {object} event - the event that is to be added to the event stream to be matched
       * @memberof MTLG.lightParse#
       */
      var doStep = function(event){

            //Handle time- specific event if timeoutTrigger is activated
            if(timeoutTrigger){
                console.log("Timeout Trigger was set") // DEBUG
                var timeNow = (new Date()).getTime();
                //check which (possibly several) timeouts have ended
                for(timeInfo in timeInformation){
                    var currentEvent = timeInformation[timeInfo];
                    if(!currentEvent){
                        continue;
                    }
                    //is the time for this event over?
                    if(currentEvent.endTime <= timeNow){
                        //revert state if partner was not found
                        //This is a different process depending on the matching strategy
                        switch(automata[currentEvent.automatonLabel].strategy){
                          case "parallel":
                            if(!isInTransitiveClosure(currentEvent.automatonLabel, currentEvent.user, currentEvent.partner)) { //TODO: Check
                                automata[currentEvent.automatonLabel].currentStates[currentEvent.user].at = currentEvent.oldAt;
                                console.log("Reverting State"); //DEBUG
                            }
                            delete timeInformation[timeInfo];
                            break;
                          case "general": //TODO
                            break;
                          default: //parallel
                            if(!isInTransitiveClosure(currentEvent.automatonLabel, currentEvent.user, currentEvent.partner)) { //TODO: Check
                                automata[currentEvent.automatonLabel].currentStates[currentEvent.user].at = currentEvent.oldAt;
                                console.log("Reverting State"); //DEBUG
                            }
                            delete timeInformation[timeInfo];
                        }
                    }

                }
                timeoutTrigger = false;
            }

            //Iterate through all automata and try matching the new event
            for(currentAutomatonLabel in automata){
                currentAutomaton = automata[currentAutomatonLabel];
                //Check if currentAutomaton was deleted, continue if this is the case
                if(!currentAutomaton){
                    //console.log("Automaton at: " + currentAutomatonLabel + " is empty!"); //DEBUG
                    continue;
                }


                //Check what kind of matching strategy is employed and act accordingly
                switch(currentAutomaton.strategy){
                  case "parallel": doParallelStep(currentAutomatonLabel,event);
                    break;
                  case "general": doGeneralStep(currentAutomatonLabel,event);
                    break;
                  default: //parallel
                    doParallelStep(currentAutomatonLabel);
                }

            }
        };

      return {
          //savedAutomata : automata, //DEBUG
          addAutomaton : addAutomaton ,
          deleteAutomaton : deleteAutomaton,
          deleteAllAutomata : deleteAllAutomata,
          doStep : doStep,
      }
  })();



  /*
   * Second Sub-module of lightParse.
   * The Parser is responsible for transforming patterns into the JavaScript representation of an automaton.
   * This is done by calling the addPattern method.
   */
  var parser = (function f() {

    //TODO: Unnecessary?
    /*
    var seqMatcher = /[Ss][Ee][Qq]/m; //Matches SEQ caseinsensitively allowing (unnecessarily for multiline matches). TODO: Use /i instead?
    var tMatcher = /[Tt]/m;
    var whereMatcher = /[Ww][Hh][Ee][Rr][Ee]/m;
    var varMatcher = /[a-z][\w]*$/m; //TODO: $ at the end...
    var wordBracketMatcher = /[\w()]/m;
    var spaceMatcher = /[\s\n]/m;
    var wordMatcher = /\w/m;
    var commaMatcher = /,/m;
    var wordCommaBracketMatcher = /[\w,()]/m;
    var wordBracketEqualMatcher = /[\w=()]/m;
    var numberMatcher = /\d/m;
    var wordDotMatcher = /[\w.]/m;
    var varExtendedMatcher = /[a-z][\w.]*$/m; //TODO: $ at end
    */

    //RegEx needed by the parser
    var termMatcher = /[\w.+*\/\-=><\s\n\"'!]/m; //Matches chars that can be included in a WHERE-term
    var userMatcher = /\w*[\w.]*.User/i; //Matches exactly the user variable in an event TODO
    var timeMatcher = /^\w*(.\w+)*.Time$/i; //Matches exactly the time attribute in an event TODO


    var localAutomaton; //Contains the Automaton in its current state, as it is created. Used to add conditions in whereTranslator, states in addPattern and by the tests.


    /*
      Returns an array of tokens for WHERE-terms from the input provided as a string.
    */
    var lexer = function(input){
        var ret = []; //Array containing the tokens
        var currentPos = 0; //Current position in WHERE-term
        //Iterate through chars in input
        while(currentPos < input.length){
            var currentChar = input.charAt(currentPos);
            //Check for words
            if( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') ){
                var currentVariable = "";
                while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') || (currentChar === '.') || (currentChar >= '0' && currentChar <= '9') ){
                    currentVariable += currentChar;
                    currentPos ++;
                    currentChar = input.charAt(currentPos);
                }
              ret[ret.length] = {
                  name : "Variable",
                  attribute : currentVariable,
              }
            //Check for numbers
            } else if ( currentChar >= '0' && currentChar <= '9') {
            // else if (!isNaN(currentChar))
                var currentNumber = 0;
                while( currentChar >= '0' && currentChar <= '9'){
                //while(!isNaN(currentChar))
                    currentNumber *= 10;
                    currentNumber += +currentChar;
                    currentPos ++;
                    currentChar = input.charAt(currentPos);
                }
                ret[ret.length] = {
                  name : "Number",
                  attribute : currentNumber,
                }
            }
            //Check for Strings (to compare member variables to)
            else if( currentChar === '"' || currentChar === "'"){
                var currentString = "";
                var bufferCurrentChar = currentChar; //remember what kind of quotation mark was used
                currentPos++; //but don't put it into the string
                currentChar = input.charAt(currentPos);
                //while read char != opening quotation mark
                while(currentChar !== bufferCurrentChar){
                    currentString += currentChar; //add char to string
                    currentPos ++;
                    currentChar = input.charAt(currentPos);
                }
                ret[ret.length] = {
                    name : "String",
                    attribute : currentString,
                }
                currentPos ++;
            }
            //Check for opening parenthesis
            else if (currentChar === '('){
                ret[ret.length] = {
                  name : "Parentheses",
                  attribute : '(',
                }
                currentPos ++;
            }
            //Next check for closing parenthesis
            else if (currentChar === ')'){
                ret[ret.length] = {
                  name : "Parentheses",
                  attribute : ')',
                }
                currentPos ++;
            }
            //Check for arithmetic symbols
            else if (currentChar === '+' || currentChar === '*' || currentChar === '/' || currentChar === '-' ){
                ret[ret.length] = {
                    name : "ArithOperator",
                    attribute : currentChar,
                }
                currentPos ++;
            }
            //Check for comparison operators.
            else if(currentChar === '>' || currentChar === '<' || currentChar === '=' || currentChar === '!'){
                var nextChar = input.charAt(currentPos + 1);
                if(currentChar === '>' && nextChar !== '='){
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : ">",
                    }
                }
                if(currentChar === '<' && nextChar !== '='){
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : "<",
                    }
                }
                if(currentChar === '>' && nextChar === '='){
                    currentPos ++;
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : ">=",
                    }
                }
                if(currentChar === '<' && nextChar === '='){
                    currentPos ++;
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : "<=",
                    }
                }
                //Change = and == to ===
                if(currentChar === '=' && nextChar === '='){
                    currentPos ++;
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : "===",
                    }
                }
                if(currentChar === '=' && nextChar !== '='){
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : "===",
                    }
                }
                //change != to !===
                if(currentChar === '!' && nextChar === '='){
                    currentPos ++;
                    if(input.charAt(currentPos + 2) === '='){
                        currentPos ++;
                    }
                    ret[ret.length] = {
                        name : "CompOperator",
                        attribute : "!==",
                    }
                }

                currentPos ++;
            }
            //Check for the delimiting ','
            else if(currentChar === ','){
                ret[ret.length] = {
                    name : "Delimiter",
                    attribute : ",",
                }
            }
            //Ignore other symbols (spaces)
            else {
                currentPos ++;
            }
        }
        //Finished with for-loop, retuning array of tokens

        return ret;

    }
    /*
     * Input is the array of tokens created by the lexer function.
     * whereTranslator adds the corresponding function to the localAutomaton.
     * Checks for user specific events, adds isUserSpecific.
     * Checks for time-specific events and adds timeInformation.
     */
    var whereTranslator = function(input){
        var ret = "return "; //String representation of function, only consists of return in the beginning
        var occurringVarNames = []; //Contains all the occurring variable names, used to add the condition to the correct state
        var occurringAttributes = {}; //Contains exact path to occurring attributes. Used to add this information to states where there is comparison between two events.
        var isUserSpecific = false; //Remember if some attribute was compared to the user attribute in the event
        var isTimeSpecific = false; //Same for time comparisons

        //Iterate through array of tokens and generate function as string
        for(var i = 0; i < input.length; i++){
            var currentToken = input[i];
            var currentName = currentToken.name;
            var currentAttribute = currentToken.attribute;
            //Special Case: Variables
            if(currentName === "Variable"){
                //Check if this var is userSpecific, by using the RegEx that matches the user attribute
                if(userMatcher.test(currentAttribute)){
                    isUserSpecific = true; //We found a comparison to user attribute, all occurring vars will have to be set to userSpecific
                //Check if this var is a time comparison using the corresponding RegEx
                } else if(timeMatcher.test(currentAttribute)){
                    isTimeSpecific = true;
                    // All Time logic here TODO: Recursive descent or something this
                    // TODO: check if input has enough entries.

                    //Get the name of the first event
                    var firstEventName = currentAttribute;
                    //make sure next symbol is '+'
                    i ++;
                    var expectedPlus = input[i];
                    var expectedPlusToken = expectedPlus.name;
                    var expectedPlusAttribute = expectedPlus.attribute;
                    //followed by a number
                    i ++;
                    var expectedNumber = input[i];
                    var expectedNumberToken = expectedNumber.name;
                    var expectedNumberAttribute = expectedNumber.attribute;
                    //followed by '<' of '>'
                    i ++;
                    var expectedGeq = input[i];
                    var expectedGeqToken = expectedGeq.name;
                    var expectedGeqAttribute = expectedGeq.attribute;
                    //lastly another time attribute
                    i ++;
                    var expectedTime = input[i];
                    var expectedTimeToken = expectedTime.name;
                    var expectedTimeAttribute = expectedTime.attribute;
                    i++;
                    if(i < input.length){
                        var expectedDelim = input[i];
                        var expectedDelimToken = expectedDelim.name;
                        var expectedDelimAttribute = expectedDelim.attribute;
                        // console.log("i is now: " + i); //DEBUG
                    } else {
                        var expectedDelimToken = "Delimiter";
                        var expectedDelimAttribute = ",";
                        // console.log("i is now: " + i + "/" + input.length); //DEBUG
                    }
                    /*
                    //DEBUG
                    console.log(expectedPlusToken);
                    console.log(expectedNumberToken);
                    console.log(expectedGeqToken);
                    console.log(expectedTimeToken);
                    console.log(expectedDelimToken);
                    console.log(expectedPlusAttribute);
                    console.log(expectedNumberAttribute);
                    console.log(expectedGeqAttribute);
                    console.log(expectedTimeAttribute);
                    console.log(expectedDelimAttribute);
                    //DEBUG
                    */
                    //Check if all the tokens are correct
                    if(expectedPlusToken !== "ArithOperator" || expectedPlusAttribute !== '+'
                          || expectedNumberToken !== "Number"
                          || expectedGeqToken !== "CompOperator" //|| expectedGeqAttribute !== ">"
                          || expectedTimeToken !== "Variable" || ! timeMatcher.test(expectedTimeAttribute)
                          || expectedDelimToken !== "Delimiter" || expectedDelimAttribute !== ","){
                        //ERROR HERE
                        console.log("Error parsing where part: Time specific events have to be of a certain form. See docs for more info.");
                        throw "Invalid Syntax"; //Caught in parser, this part will simply be ignored
                    } else { //Tokens are correct
                        //Add information to first state, complex case with timeout
                        if(expectedGeqAttribute === ">") {
                          var firstVarName = firstEventName.split(".")[0]; //Split to access event identifier
                          var secondVarName = expectedTimeAttribute.split(".")[0];
                          //Check if varNames actually occured in SEQ-part
                          if(!localAutomaton.states[firstVarName] || !localAutomaton.states[secondVarName]){
                              console.log("Trying to access undefined var name '" + firstVarName + " or " + secondVarName + "' in WHERE part. Aborting!");
                              throw "Undefined var in WHERE";
                          }
                          //Add time information. Create times if it does not exist already
                          localAutomaton.states[firstVarName].times = localAutomaton.states[firstVarName].times || [];
                          localAutomaton.states[firstVarName].times.push({time : expectedNumberAttribute, partner : secondVarName});
                          isTimeSpecific = true; //Remember that this was a time comparison and do not add the condition to any state TODO: Maybe add the condition nonetheless?
                        } else if (expectedGeqAttribute === "<") { //Just add the rule, easy case

                          //Create string representing first time attribute
                          var variable1 = 'args["' + currentAttribute.split(".")[0] + '"]'; //will only look for the first member
                          for (var j = 1; j < currentAttribute.split(".").length; j++){ //now we add all the other members
                              variable1 = variable1 + '["' + currentAttribute.split(".")[j] + '"]';
                          }

                          //Create string representing second time attribute
                          var variable2 = 'args["' + expectedTimeAttribute.split(".")[0] + '"]'; //will only look for the first member
                          for (var j = 1; j < expectedTimeAttribute.split(".").length; j++){ //now we add all the other members
                              variable2 = variable2 + '["' + expectedTimeAttribute.split(".")[j] + '"]';
                          }

                          //Append check for time attributes
                          ret = "try{" + variable1+ "}catch(e){console.log('Something went wrong while trying to access " + variable1+ "'); return false; }\n" + ret;
                          ret = "try{" + variable2+ "}catch(e){console.log('Something went wrong while trying to access " + variable2+ "'); return false; }\n" + ret;
                          //Create comparison
                          ret = ret + variable1 + " + " + expectedNumberAttribute + " < " + variable2;
                          //console.log(ret); //DEBUG
                          //Create function and add it to condition array if possible
                          try{
                            localAutomaton.states[expectedTimeAttribute.split(".")[0]].condition.push(Function("args",ret));
                          } catch (e) {
                            console.log("Something went wrong trying to create a time comparison. Ignoring this part. ");
                            return;
                          }
                        }
                    }
                    //end this loop
                    continue; //TODO: Edge case, something with max-int long input might break here

                }

                //handle var names with splitting on "." to access objects or attributes in objects
                var buffer = currentAttribute.split(".");
                var variable = ""; //This variable will contain the final var name
                var variableAttribute = []; //Array containing the path to the current attribute, without args and var-name
                //add event name of current variable to occurringVarNames array
                occurringVarNames[occurringVarNames.length] = buffer[0];
                if(!localAutomaton.states[buffer[0]]){
                    console.log("Trying to access undefined var name '" + buffer[0] + "' in WHERE part. Aborting!");
                    throw "Undefined var in WHERE";
                }
                //Create var name
                variable = 'args["' + buffer[0] + '"]'; //will only look for the first member
                //ret = ret + 'args["' + buffer[0] + '"]';
                for (var j = 1; j < buffer.length; j++){ //now we add all the other members
                    variable = variable + '["' + buffer[j] + '"]';
                    variableAttribute.push(buffer[j]);
                    //ret = ret + '["' + buffer[j] + '"]';
                }
                occurringAttributes[buffer[0]] = occurringAttributes[buffer[0]] || [];
                occurringAttributes[buffer[0]].push(variableAttribute);
                ret = ret + variable; //add the variable to the end of our function
                //also check if we are trying to access undefined values, which would result in errors. Make this false instead
                ret = "try{" + variable + "}catch(e){console.log('Something went wrong while trying to access " + variable + "'); return false; }\n" + ret;

            }
            //Easy cases: Simply append current attribute to the string representation
            else if (currentName === "Number"){
                ret = ret + currentAttribute;
            }
            else if (currentName === "Parentheses"){
                ret = ret + currentAttribute;
            }
            else if (currentName === "ArithOperator"){
                ret = ret + currentAttribute;
            }
            else if (currentName === "CompOperator"){
                ret = ret + currentAttribute;
            }
            else if (currentName === "String"){
                ret = ret + '"' + currentAttribute + '"';
            }
            //Delimiter represents end of this term
            else if (currentName === "Delimiter") { //TODO: This is never executed right now (due to the way this function is called)
                //Create function
                finishWhereTranslation(isUserSpecific, isTimeSpecific, occurringVarNames, ret, occurringAttributes);
                occurringVarNames = []; //Reset the occurring names for next clause
            }
        } //end of for loop

        //Create function
        finishWhereTranslation(isUserSpecific, isTimeSpecific, occurringVarNames, ret, occurringAttributes);
    }

    /*
     * Helper function called by whereTranslator to create the function and add it to the correct state.
     */
    var finishWhereTranslation = function (isUserSpecific, isTimeSpecific, occurringVarNames, ret, occurringAttributes) {
        //Add isUserSpecific to all occurring states if there was a user comparison
        if(isUserSpecific){
            for(var m = 0; m < occurringVarNames.length; m++){
                localAutomaton.states[occurringVarNames[m]].isUserSpecific = true;
            }
            isUserSpecific = false;
        }

        //Return if time specific (ret is empty)
        if(isTimeSpecific){
            isTimeSpecific = false; //Unnecessary
            return;
        }

        //Add information about attributes that will be compared to other events later
        //Check if there are more than two events in the comparison (by checking occurringVarNames)
        var firstOccurring = occurringVarNames[0];
        var moreThanOne = false;
        for(var i = 1; i < occurringVarNames.length; ++i) {
          if(occurringVarNames[i] !== firstOccurring){
            moreThanOne = true;
            break;
          }
        }
        if(moreThanOne) { //Found more than one var name
          //Iterate occurring var names and add the information to the state
          for(var i = 0; i < occurringVarNames.length; ++i){
            if(localAutomaton.states[occurringVarNames[i]].interComparisons) { //Array exists, add content of current array to it
              localAutomaton.states[occurringVarNames[i]].interComparisons = localAutomaton.states[occurringVarNames[i]].interComparisons.concat(occurringAttributes[occurringVarNames[i]]);
            } else { //Make current array the new one
              localAutomaton.states[occurringVarNames[i]].interComparisons = occurringAttributes[occurringVarNames[i]];
            }
          }
        }

        //Find the correct state to add the condition to
        //by calculating the transitive closure of all states
        for(var k = 0; k <= occurringVarNames.length -2; k++){ //iterate through all states: k
            for(var l = k + 1; l <= occurringVarNames.length - 1; l++){ //iterate states after that: l
                //console.log("In for loop with k: " + k + " and l: " + l); //DEBUG
                //console.log(occurringVarNames); //DEBUG
                //Check if l comes after k
                if(occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[k], occurringVarNames[l])){
                    //It does, so we can delete k from the array (we want to add the condition to the LAST state)
                    occurringVarNames.splice(k,1);
                    //console.log("In case with k: " + k + " and l: " + l); //DEBUG
                    //console.log(occurringVarNames); //DEBUG
                    //We deleted the k-th element so we have to check the new k-th element next
                    k--;
                    break;
                //Mirror case: l comes before k
                } else if (occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[l], occurringVarNames[k])){
                  //delete l then
                    occurringVarNames.splice(l,1);
                    l--;
                }
            }
        }

        try{
          //Try creating the function using the Function constructor, with an object containing the arguments called args
          localAutomaton.states[occurringVarNames[0]].condition.push(Function("args",ret));
        }catch (e) {
          //failed, must have been something wrong with the WHERE-term
            console.log("Unable to parse where statement: \n'" + ret + "'. \nThis part will be ignored");
        }
        //console.log(ret); //DEBUG
    }
    /*
     * This function returns true if secondName is in the transitive closure of firstName
     */
    var isInTransitiveClosure = function(firstName, secondName) {
        //Retrun false if the firstName state doesn't exist
        if(!localAutomaton.states[firstName]){
            //Error
            return;
        }
        //Return true if the names are equal
        if(firstName === secondName) {
          return true;
        }
        var currentName = firstName;
        while(localAutomaton.states[currentName].nextState){
            if(localAutomaton.states[currentName].nextState === secondName){
                return true;
            }
            currentName = localAutomaton.states[currentName].nextState;
        }
        return false;
    }


    var localInput = ""; //local version of input pattern
    var localCurrentPos = 0; //current position in input

    /*
     * Sets the localInput to input and resets the localCurrentPos to 0
     */
    var initializeLocalInput= function (input){
      localInput = input;
      localCurrentPos = 0;
    }

    /*
     * Returns the next word in localInput from localCurrentPos
     */
    var ruleGetNextWord = function(){
      var buffer = detectWord(localCurrentPos);
      localCurrentPos = buffer.pos;
      return buffer.returnObject;
    }

    /*
     * Returns the next WHERE-term starting from localCurrentPos.
     */
    var ruleGetNextWherePart = function(){
      var ret = "";
      while(localCurrentPos < localInput.length){
        var currentChar = localInput.charAt(localCurrentPos);
        if(termMatcher.test(currentChar)){
          ret += currentChar;
          localCurrentPos++;
        } else if (currentChar === "("){ //Opening parenthesis found, start special routine
          //console.log("Entering `(` part"); //DEBUG
          ret += currentChar;
          localCurrentPos ++;
          ret += ruleGetNextWherePart(); //Search until closing bracket was found
          currentChar = localInput.charAt(localCurrentPos); //Append found word
          if( currentChar !== ')' ) {
            throw "Not enough closing brackets in WHERE part! Aborting this pattern";
          } else {
            ret += currentChar;
            localCurrentPos ++;
          }
        } else if (currentChar === ")" || currentChar === ","){
          //console.log(ret); //DEBUG
          return ret;
        } else {
          throw "Parser Error: Unknown symbol: " + currentChar;
        }



      }
      throw "Unexpected End of Where part! Aborting this patter: " + ret;
      return "";
    };


    /*
     * Used by ruleGetNextWord and rulePeekNextWord to find the next word.
     * Returns word and new position in an object.
     */
    var detectWord = function (position) {
      var currentPos = position;
      while(currentPos < localInput.length){
        var currentChar = localInput.charAt(currentPos);
        //Check for words
        if( (currentChar >= 'a' && currentChar <= 'z') ) { //Vars
          var currentWord = "";
          while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') || (currentChar >= '0' && currentChar <= '9') ) {
            currentWord += currentChar;
            currentPos ++;
            currentChar = localInput.charAt(currentPos);
          }
          return {pos : currentPos, returnObject : {name: "Variable",
                  attribute: currentWord,
          },
          };
        } else if( (currentChar >= 'A' && currentChar <= 'Z') ){ //Control commands
          var currentWord = "";
          while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') ){
            currentWord += currentChar;
            currentPos ++;
            currentChar = localInput.charAt(currentPos);
          }
          switch (true) {
            case /^seq$/i.test(currentWord):
              return {pos: currentPos, returnObject: {name: "SEQ",
                        attribute: "",
              },
              };
              break;
            case /^where$/i.test(currentWord):
              return {pos: currentPos, returnObject : {name: "WHERE",
                        attribute: "",
              },
              };
              break;
            case /^users$/i.test(currentWord):
              return {pos: currentPos, returnObject : {name: "USERS",
                        attribute: "",
              },
              };
              break;
            case /^strategy$/i.test(currentWord):
              return {pos: currentPos, returnObject : {name: "STRATEGY",
                        attribute: "",
              },
              };
              break;
            default:
              //throw "Unknown command sequence: " + currentWord;
              return {pos: currentPos, returnObject : {name: "ERROR",
                attribute: currentWord,
              },
              };
          }
        //Check for parentheses
        } else if (currentChar === '(' || currentChar === ')' ) {
          currentPos++;
          return {pos: currentPos, returnObject : {name: "Parentheses",
                    attribute: currentChar
          },
          };
          //currentPos ++;
        //Check for delimiters (,)
        } else if (currentChar === ',') {
          currentPos++;
          return {pos: currentPos, returnObject : {name: "Delimiter",
            attribute: ',',
          },
          };
        //Check for numbers
        } else if (currentChar >= '0' && currentChar <= '9'){
          var currentWord = 0;
          while (currentChar >= '0' && currentChar <= '9') {
            currentWord *= 10;
            currentWord += +currentChar;
            currentPos ++;
            currentChar = localInput.charAt(currentPos);
          }
          return {pos : currentPos, returnObject : {name: "Number",
                  attribute: currentWord,
          },
          };
        //Check for equal signs (assignments)
        } else if (currentChar === '=') {
          currentPos++;
          return {pos : currentPos, returnObject : {name: "ComparisonOperator",
                  attribute: '=',
          },
          };
        //Remove spaces etc
        } else {
          currentPos ++;
        }
      }

      return {pos: currentPos, returnObject : {name: "END",
              attribute: "",
      },
      };

    }

    /*
     * Returns next token in pattern without consuming it. Needed for Look-ahead of one.
     */
    var rulePeekNextWord = function(){
      var currentPos = localCurrentPos;
      return detectWord(currentPos).returnObject;
    }


    /*
      Create and output Generic Error message to console
    */
    var parserError = function (mode, pattern, position){
      pattern = pattern || localInput;
      position = position || localCurrentPos; //TODO: Might be one too far
        console.log("Error parsing " + mode + " part in \n'"
              + pattern + "'\nat position: " + position
              +" (char: " + pattern.charAt(position) + ").");
    }



    /**
     * @function addPattern
     * @description The addPattern function translates the pattern in the first parameter into an automaton, and passes it to the automaton-handler.
     * The second argument is the callback function that is invoked when the pattern is matched.
     * If no callback function is specified the automaton is still created for testing purposes.
     * @param {string} pattern - the pattern the new automaton will search for in the event stream
     * @param {function} callbackFunction - the function that is called when the pattern as found. The function will receive information about the matched events as parameter.
     * @return {number}  Returns the index of the automaton in the automata array. Use this number to delete or modify the automaton later.
     * @memberof MTLG.lightParse#
     */
    var addPattern = function (pattern, callbackFunction){
          var mode = "Start";//parsing mode: Start -> in SEQ -> AfterSEQ -> (KEYWORDS)-> afterSeq -> (KEYWORDS) -> ... -> Finished
          //var position = 0; //position in pattern
          //var patternCpy = JSON.parse(JSON.stringify(pattern)); //create a copy to make sure the original is preserved (for error handling)
          var currentMatch; // current word as received by the "Lexer"
          //Create empty callback if none is specified. Useful when testing and forgetting to add a function...
          callbackFunction = callbackFunction || function () {};
          localAutomaton = {states: { Start : {condition : []} }, size : 1, callback : callbackFunction, strategy : "parallel", }; //The Automaton that will be given to the Automaton handler in the end TODO: Is the condition is start necessary?
          initializeLocalInput(pattern); //set localInput to pattern and localCurrentPos to 0
          var lastState = "Start"; //Remember the last state for transitions
        do{ //This do loop will go through the pattern and parse the next word according to the current mode
          //console.log(mode); //DEBUG
          switch(mode){
              case "Start": //At the very beginning of the pattern
                currentMatch = ruleGetNextWord();
                //Has to start with the SEQ-part
                if(currentMatch.name !== "SEQ"){
                  parserError(mode);
                  mode = "Finished";
                  localAutomaton = null;
                } else {
                  currentMatch = ruleGetNextWord();
                  //SEQ-part has to start with '('
                  if(currentMatch.name !== "Parentheses" || currentMatch.attribute !== "("){
                    parserError(mode);
                    mode = "Finished";
                    localAutomaton = null;
                  } else {//OK, successfully found SEQ-part beginning
                    //console.log("Found SEQ("); //DEBUG
                    mode = "inSeq";
                    //console.log(currentMatch); //DEBUG
                    //console.log(rulePeekNextWord()); //DEBUG
                  }
                }
                  break;
              case "inSeq": //after reading SEQ(
                currentMatch = ruleGetNextWord();
                //console.log(currentMatch); //DEBUG
                //Only accept var-names in SEQ-part
                if(currentMatch.name !== "Variable"){
                  //console.log(currentMatch); //DEBUG
                  //If this is the last var-name
                  if(currentMatch.name === "Parentheses" && currentMatch.attribute === ')'){
                    //console.log("Ending SEQ- part"); //DEBUG
                    mode = "afterSeq";
                  } else if (currentMatch.name === "Delimiter" && currentMatch.attribute === ',') {
                    //Consume delimiters, next var name is added in next loop round
                  }else {//Neither delimiter nor closing parenthesis
                    parserError(mode);
                    mode = "Finished";
                    localAutomaton = null;
                  }
                } else { //Everything OK with the var name, add a corresponding state
                  if(localAutomaton.states[currentMatch.attribute]) { //State already exists, double naming
                    parserError(mode);
                    mode = "Finished";
                    localAutomaton = null;
                  } else { //create new state, name is valid
                    localAutomaton.states[currentMatch.attribute]={condition : [] }; //Create new state
                    if(lastState) { //remember previous and next state
                      localAutomaton.states[lastState].nextState = currentMatch.attribute;
                      localAutomaton.states[currentMatch.attribute].previousState = lastState;
                    }
                    lastState = currentMatch.attribute; //update last state info
                    var bufferWord = rulePeekNextWord(); //Look-ahead: is this the last variable? Set final state then
                    if(bufferWord.name === "Parentheses" && bufferWord.attribute === ')'){
                      localAutomaton.states[currentMatch.attribute].final = true;
                    }
                    localAutomaton.size ++;
                  }
                }
                break;
              case "afterSeq": //read "SEQ(...)", now waiting for either "WHERE" or (KEYWORD)
              currentMatch = ruleGetNextWord();
              switch(currentMatch.name){
                case "WHERE": //Found WHERE
                  var bufferWord = ruleGetNextWord();
                  if(bufferWord.name !== "Parentheses" || bufferWord.attribute !== '('){
                    parserError(mode); //WHERE part starts with (
                    mode = "Finished";
                    localAutomaton = null;
                  } else {
                    mode = "inWhere";
                  }
                  break;
                case "STRATEGY":
                  console.log("STRATEGY-keyword detected"); //DEBUG
                  mode = "Strategy";
                  //Read Strategy
                  var bufferedWord = ruleGetNextWord();
                  if(bufferedWord.name !== "ComparisonOperator" || bufferedWord.attribute !== '='){ //Error case
                    parserError(mode);
                    mode = "Finished";
                    localAutomaton = null;
                  } else { //Check if next word is variable
                    bufferedWord = ruleGetNextWord();
                    if(bufferedWord.name !== "Variable") { //Error case
                      parserError(mode);
                      mode = "Finished";
                      localAutomaton = null;
                    } else {//next word is variable, check if this strategy exists
                      switch(bufferedWord.attribute){
                        case "parallel": localAutomaton.strategy = "parallel";
                          break;
                        case "general": localAutomaton.strategy = "general";
                          break;
                        default:
                          console.log("Unknown matching strategy: " + bufferedWord.attribute);
                      }
                      //Assume that strategy part is over, go back to afterSeq
                      mode = "afterSeq";
                    }
                  }
                  break; //End STRATEGY-keyword
                case "END": //Finished parsing
                  console.log("Reached end of pattern. Automaton created successfully!"); //DEBUG
                  mode = "Finished";
                  break;
                default:
                  console.log("Encountered unknown control sequence. Aborting!"); //TODO
                  mode = "Finished";
                  localAutomaton = null;
              }
              break;
              case "inWhere": // found "SEQ(...)WHERE("
                //console.log("Entering inWhere"); //DEBUG
                //Try getting WHERE- part. Might throw errors due to wrong parentheses, ...
                try{
                  currentMatch = ruleGetNextWherePart(); //Returns the term without ending , or )
                }
                catch (error){
                  console.log("Error while reading WHERE- term: " + error);
                  console.log("Ignoring this part");
                  currentMatch = null;
                }
                if(currentMatch){
                  //console.log("Translating inWhere"); //DEBUG
                  //Try createing the function
                  try{
                    whereTranslator(lexer(currentMatch));
                  } catch (error) {
                    console.log("Error translating Where part: " + error);
                  }
                  //console.log("Finished Translating inWhere"); //DEBUG
                }

                var bufferWord = ruleGetNextWord();
                if (bufferWord.name === "Delimiter"){
                  //Consume Delimiter
                } else if (bufferWord.name === "Parentheses") { //This was the last term
                  mode = "afterSeq";
                } else if (bufferWord.name === "END") { //Avoid problems while creating last WHERE term (missing closing bracket?)
                  mode = "Finished";
                }
                break;
            }
        }while(mode != "Finished"); //End of do-while-loop
        //Was the automaton created successfully?
          if(!localAutomaton) {
            console.log("Failed to create automaton");
            return -1;
          }
          console.log(localAutomaton); //DEBUG
          var automatonNumber = handler.addAutomaton(localAutomaton); //Pass the Automaton we created for this pattern on to the Automaton handler
          //console.log(handler.savedAutomata); //DEBUG
          return automatonNumber;
      }



    return{

        /*
          This function takes a pattern and callback function as arguments and creates, as well as registers an Automaton that matches the given pattern.
        */
      addPattern: addPattern,

    }

  })();

  return{
    addPattern: parser.addPattern,
    matchEvent: handler.doStep,
    deleteAutomaton : handler.deleteAutomaton,
    deleteAllAutomata : handler.deleteAllAutomata,
    setNumberOfUsers : handler.setNumberOfUsers,

  }

})()

function init(){
  //Nothing to do here
}

MTLG.lightParse = lightParse;
MTLG.addModule(init);
